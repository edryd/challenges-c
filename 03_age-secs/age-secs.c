#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int
main(int argc, char *argv[]) {
	struct tm *tm_date; /* birthdate as a tm struct */
	int u_date; /* birthdate in unix time */
	int curtime = time(0);

	if (argc != 4) {
		printf("Usage: age-secs YYYY MM DD\n");
		printf("Enter birthdate and get how old you are in seconds\n");
		return 1;
	}

	/* init tm_date */
	time_t rawtime;
	time(&rawtime);
	tm_date = localtime(&rawtime);

	/* assign tm_date */
	tm_date->tm_year = atoi(argv[1]) - 1900;
	tm_date->tm_mon  = atoi(argv[2]) - 1;
	tm_date->tm_mday = atoi(argv[3]);
	tm_date->tm_hour = 0;
	tm_date->tm_min  = 0;
	tm_date->tm_sec  = 0;
	u_date = mktime(tm_date); /* convert tm_date to unix time */

	printf("%d\n", curtime - u_date);

	return 0;
}
