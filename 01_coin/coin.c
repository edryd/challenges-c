#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int
main(void) {
	srand(time(NULL));

	printf("%s\n", (rand() % 2 == 0) ? "heads" : "tails");

	return 0;
}
