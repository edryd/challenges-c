#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_NAMES 5

char *firsts[MAX_NAMES] = { "John",  "James", "Henry", "Bob",     "Michael"  };
char *lasts[MAX_NAMES]  = { "Smith", "doe",   "Smith", "Johnson", "Williams" };

int
main(void) {
	char *first;
	char *middle;
	char *last;
	srand(time(NULL));

	first = firsts[rand()%MAX_NAMES];
	while (middle = firsts[rand()%MAX_NAMES], strcmp(middle, first) == 0);
	last = lasts[rand()%MAX_NAMES];

	printf("%s %s %s\n", first, middle, last);

	return 0;
}
