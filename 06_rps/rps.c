#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int
main(void) {
	char choices[3] = { 'r', 'p', 's' };
	char comp, player;
	srand(time(NULL));

	printf("Rock, Paper, or Scissors? (r/p/s) ");
	scanf("%c", &player);

	comp = choices[rand() % 3];

	switch (player) {
	case 'r':
	case 'R':
		switch (comp) {
		case 'r':
			printf("The computer chose rock, you tied!\n");
			break;
		case 'p':
			printf("The computer chose paper, you lost!\n");
			break;
		case 's':
			printf("The computer chose scissors, you won!\n");
			break;
		}
		break;
	case 'p':
	case 'P':
		switch (comp) {
		case 'r':
			printf("The computer chose rock, you won!\n");
			break;
		case 'p':
			printf("The computer chose paper, you tied!\n");
			break;
		case 's':
			printf("The computer chose scissors, you lost!\n");
			break;
		}
		break;
	case 's':
	case 'S':
		switch (comp) {
		case 'r':
			printf("The computer chose rock, you lost!\n");
			break;
		case 'p':
			printf("The computer chose paper, you won!\n");
			break;
		case 's':
			printf("The computer chose scissors, you tied!\n");
			break;
		}
		break;
	default:
		printf("Not a recognized options. r, p, and c are the only valid inputs\n");
		break;
	}

	return 0;
}
