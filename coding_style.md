# Style Guide
Most of this is personal preference, but it really does not matter what it is,
it just need to be consistent.  When in doubt about some code, just look through
the rest of the source to see examples.

Modified from <http://suckless.org/coding_style>

## File Layout
1. Comment with LICENSE and possibly short explanation of file/tool
2. Headers
	* Place system headers first in alphabetical order
		* If headers must be included in a specific order comment to explain
	* Place local headers after an empty line
3. Macros
4. Types
5. Function declarations
	* Include variable names
	* Include short description
	* For short files these can be left out
	* Group/order in logical manner
6. Global variables
7. Function definitions in same order as declarations
8. `main`

## General
* Use C99
* Do not mix declarations and code
* Use `/* */` for comments, not `//`
* Only accepted build systems are mk or gnu make
* Use tabs for indentation
* Use spaces for alignment
* Never have more then 3 levels of indentation
* 80 character line limit, not including comments
* Put spaces after commas and around operations

## Blocks
* All variable declarations at top of block
* `{` on same line preceded by single space
* `}` on own line unless continuing statement (`if else`, `do while`, ...)

## Functions
* Functions should not be longer than your screen height, if you can not see
  the entire function at once then it is too long
* Above function should be short description
* Return type and modifiers on own line
* Function name and argument list on next line
* Opening `{` on same line
* Functions not used outside translation unit should be declared and defined
  `static`

## Variables
* Global variables not used outside translation unit should be declared `static`
* In declaration of pointers the `*` is adjacent to variable name, not type

## Keywords
* Use a space after `if`, `for`, `while`, `switch` (they are not function calls)
* Do not use a space after the opening `(` and before the closing `)`
* Always use `()` with `sizeof`
* Do not use a space with `sizeof()` (it does act like a function call)
* Do not use `()` with `return`

## Switch
* Do not indent cases another level
* Comment cases that FALLTHROUGH

## User Defined Types
* Do not use `type_t` naming (it is reserved for POSIX and less readable)
* Typedef structs
* Do not typedef builtin types
* Capitalize the type name
* Typedef the type name, if possible without first naming the struct

		typedef struct {
			double x, y, z;
		} Point;

## Tests and Boolean Values
* Do not test against `NULL` explicitly
* Do not test against `0` explicitly
* Assign at declaration when possible

		Type *p = malloc(sizeof(*p));
		if (!p)
			hcf();
* Otherwise use compound assignment and tests unless the line grows too long

		if (!(p = malloc(sizeof(*p))))
			hcf();

## Handling Errors
* When functions `return -1` for error test against `0` not `-1`

		if (func() < 0)
			hcf();
* Use `goto` to unwind and cleanup when necessary
* `return` or `exit` early on failures instead of multiple nested levels
* Unreachable code should have a NOTREACHED comment
* Think long and hard on whether or not you should cleanup on fatal errors

## Enums vs #define
* Use enums for values that are grouped semantically and #define otherwise.

		#define MAXSZ  4096
		#define MAGIC1 0xdeadbeef

		enum {
			DIRECTION_X,
			DIRECTION_Y,
			DIRECTION_Z
		};
