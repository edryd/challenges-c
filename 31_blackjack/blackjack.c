#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

typedef int FACE;

typedef enum {
	SPADES, HEARTS, DIAMONDS, CLUBS
} SUIT;

struct card {
	FACE face;
	SUIT suit;
};

/* return what each face looks like */
static char *
face_char(FACE face) {
	switch (face) {
	case  1: return "A ";
	case  2: return "2 ";
	case  3: return "3 ";
	case  4: return "4 ";
	case  5: return "5 ";
	case  6: return "6 ";
	case  7: return "7 ";
	case  8: return "8 ";
	case  9: return "9 ";
	case 10: return "10";
	case 11: return "J ";
	case 12: return "Q ";
	case 13: return "K ";
	default: return "  ";
	}
}

/* return the unicode value for different suits */
static char *
suit_char(SUIT suit) {
	switch (suit) {
	case 0: return "♠";
	case 1: return "♡";
	case 2: return "♢";
	case 3: return "♣";
	}
	return " ";
}

/* print someone's hand */
static void
print_cards(struct card cards[5]) {
	for (int i = 0; i < 5; i++)
		if (cards[i].face != 0)
			printf("%s ", face_char(cards[i].face));
	printf("\n");
	for (int i = 0; i < 5; i++)
		if (cards[i].face != 0)
			printf("%s  ", suit_char(cards[i].suit));
	printf("\n");
}

/* return value of a face */
static int
card_val(FACE face) {
	return (face > 10) ? 10 : face;
}

/* return the sum of the values of someone's deck */
static int
add_cards(struct card cards[5]) {
	int val = 0;
	for (int i = 0; i < 5; i++)
		val += card_val(cards[i].face);
	return val;
}

int
main(void) {
	char choice;
	struct card cards[52];
	struct card comp[5] = {0}, player[5] = {0};
	srand(time(NULL));

	/* init standard deck */
	for (int num = 0; num < 52; num++) {
		cards[num].face = num % 13 + 1;
		cards[num].suit = num % 4;
	}

	/* init hands */
	comp[0] = cards[rand()%52];
	comp[1] = cards[rand()%52];
	player[0] = cards[rand()%52];
	player[1] = cards[rand()%52];

	/* set player cards */
	for (int i = 2; i < 5; i++) {
		print_cards(player);
		printf("Stay or hit? [s/h]   ");
		printf("\033[2D"); /* clear previous entry by moving left */
		scanf("%c", &choice);
		getchar();
		if (choice == 'h')
			player[i] = cards[rand()%52];
		else if (choice != 's') {
			printf("not a supported option\n");
			i--;
		}
		if (choice == 's' || add_cards(player) > 21)
			break;
		printf("\033[3A"); /* move up 3 */
	}

	int cscore = add_cards(comp), pscore = add_cards(player);

	/* show cards */
	printf("\n");
	printf("comp: %d\n", cscore);
	print_cards(comp);
	printf("player: %d\n", pscore);
	print_cards(player);

	/* determine who wins */
	if ((cscore > 21 || pscore > cscore) && pscore <= 21)
		printf("player wins\n");
	else if ((pscore > 21 || cscore > pscore) && cscore <= 21)
		printf("computer wins\n");
	else if (cscore > 21 && pscore > 21)
		printf("draw\n");
	else if (cscore == pscore)
		printf("draw\n");

	return 0;
}
